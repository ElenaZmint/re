const gulp = require('gulp');
const { series, parallel } = require('gulp');
const fileinclude = require('gulp-file-include');
const sass = require('gulp-sass')(require('sass'));
const cssnano = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const clean = require('gulp-clean');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const htmlMin = require('gulp-htmlmin');

function cleanDist() {
    return gulp.src('dist', { read: false, allowEmpty: true })
        .pipe(clean());
}

function libs() {
    return gulp.src('app/libs/**/*.*')
        .pipe(gulp.dest('dist/libs'))
}

function minJS() {
    return gulp.src('app/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))

}

function htmlMinFcn() {
    return gulp.src('dist/*.html')
        .pipe(htmlMin({
            removeEmptyAttributes: true,
            sortClassName: true,
            sortAttributes: true
        }))
        .pipe(gulp.dest('dist'));
}

function fileinc() {
    return gulp.src('app/*.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./dist'));
}

function compileSASS() {
    return gulp.src('./app/style/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./app/style'));
}

function minCSS() {
    return gulp.src('./app/style/**/*.css')
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(gulp.dest('dist/style'))
}


function img() {
    return gulp.src('./app/img/**/*.*')
        .pipe(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.mozjpeg({ quality: 80, progressive: true }),
            imagemin.optipng({ optimizationLevel: 2 }),
            imagemin.svgo({
                plugins: [
                    { removeViewBox: true },
                    { cleanupIDs: false }
                ]
            })
        ]))
        .pipe(gulp.dest('dist/img'))
}

function watchCSS() {
    return gulp.watch('./app/style/**/*.scss', series(compileSASS, minCSS));
}

function watchHTML() {
    return gulp.watch('./app/**/*.html', series(fileinc, htmlMinFcn));
}



function serve() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    watchCSS();
    gulp.watch('./app/style/**/*.scss').on('change', browserSync.reload);

    watchHTML();
    gulp.watch('./app/**/*.html').on('change', browserSync.reload);


}


exports.watchSASS = watchCSS;
exports.watchHtml = watchHTML;
exports.watchState = parallel(watchHTML, watchCSS)
exports.file = fileinc;
exports.scssToCss = compileSASS;
exports.css = minCSS;
exports.img = img;
exports.clean = cleanDist;
exports.js = minJS;
exports.html = htmlMinFcn;
exports.lib = libs;

exports.build = series(
    cleanDist,
    libs,
    minJS,
    img,
    compileSASS, minCSS,
    fileinc, htmlMinFcn,
    serve
)