$(document).ready(function () {
  $(".multiple-items").slick({
    swipeToSlide: true,
    dots: false,
    arrows: false,
    infinite: true,
    variableWidth: true,
  });
});
